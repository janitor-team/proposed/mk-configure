# A debhelper build system class for handling simple mk-configure-based projects.
#
# Copyright: © 2008 Joey Hess
#            © 2008-2009 Modestas Vainius
# License: GPL-2+

package Debian::Debhelper::Buildsystem::mkcmake;

use strict;
use Debian::Debhelper::Dh_Lib qw(compat escape_shell clean_jobserver_makeflags dpkg_architecture_value install_dir generated_file);
use base 'Debian::Debhelper::Buildsystem::makefile';

sub DESCRIPTION {
	"mk-configure"
}

sub exists_make_target {
	my ($this, $target) = @_;

	# Use -V .ALLTARGETS to get the list of targets; -n is
	# needed to avoid executing anything
	my @opts=("-n", "-V", ".ALLTARGETS");
	my $buildpath = $this->get_buildpath();
	unshift @opts, "-C", $buildpath if $buildpath ne ".";

	my $pid = open(MAKE, "-|");
	defined($pid) || error("fork failed: $!");
	if (! $pid) {
		open(STDERR, ">&STDOUT");
		$ENV{LC_ALL}='C';
		delete $ENV{MAKEFLAGS};
		exec($this->{makecmd}, @opts, @_);
		exit(1);
	}

	local $/=undef;
	my $output=<MAKE>;
	chomp $output;
	close MAKE;

	return defined $output && grep(/^$target$/, split(" ",$output));
}

# Currently, we don't want parallel build with bmake.
sub do_make {
	my $this=shift;

	# Avoid possible warnings about unavailable jobserver,
	# and force make to start a new jobserver.
	clean_jobserver_makeflags();

	my @root_cmd;
	if (exists($this->{_run_make_as_root}) and $this->{_run_make_as_root}) {
		@root_cmd = gain_root_cmd();
	}

	$ENV{MKCOMPILERSETTINGS}='yes';

	my @opts;
	my $prefix = "/usr";
	push @opts, "PREFIX=${prefix}";
	push @opts, "MANDIR=${prefix}/share/man";
	push @opts, "INFODIR=${prefix}/share/info";
	push @opts, "SYSCONFDIR=/etc";
	push @opts, "STRIPFLAG=";
	my $multiarch=dpkg_architecture_value("DEB_HOST_MULTIARCH");
	if (! compat(8)) {
		   if (defined $multiarch) {
					push @opts, "LIBDIR=${prefix}/lib/$multiarch";
					push @opts, "LIBEXECDIR=${prefix}/lib/$multiarch";
			}
			else {
					push @opts, "LIBEXECDIR=${prefix}/lib";
			}
	}
	else {
			push @opts, "LIBEXECDIR=${prefix}/lib/" . sourcepackage();
	}

	$this->doit_in_builddir(@root_cmd, $this->{makecmd}, @opts, @_);
}

sub clean {
	my $this=shift;
	if (!$this->rmdir_builddir()) {
		$this->make_first_existing_target(['cleandir', 'distclean', 'realclean', 'clean'], @_);
	}
}

sub check_auto_buildable {
	my $this=shift;
	my ($step)=@_;

	if (-e $this->get_buildpath("makefile") ||
	    -e $this->get_buildpath("Makefile"))
	{
        my $ret = ($this->SUPER::check_auto_buildable(@_));

        open (MAKEFILE, "makefile") || open (MAKEFILE, "Makefile") ||
            return 0;

        while (<MAKEFILE>) {
                chomp;
                if (/^\.?\s*include\s+<mkc/) {
                        close MAKEFILE;
                        $ret++;
                        return $ret;
                }
        }
        close MAKEFILE;
        return $ret;
	}
	return 0;
}

sub new {
	my $class=shift;
	my $this=$class->SUPER::new(@_);

	# dh creates and sets HOME automatically for compat 13+
	if (compat(12)) {
		my $cwd = Cwd::getcwd();
		my $home_dir = join('/', $cwd, generated_file('_source', 'home', 0));
		my @paths = (
			$home_dir,
		);
		install_dir(@paths);
		$ENV{'HOME'} = $home_dir;
	}

	$this->{makecmd} = "mkcmake";
	return $this;
}

1
